import * as dotenv from 'dotenv'
import * as fs from 'fs'

export interface EnvData {
  // application
  APP_ENV: string
  APP_DEBUG: boolean
  ORDER_HOST:string
  ORDER_PORT:number


  PRODUCT_HOST:string
  PRODUCT_PORT:number




  // database
  ORDER_DB_TYPE: 'mysql' | 'mariadb'
  ORDER_DB_HOST?: string
  ORDER_DB_NAME: string
  ORDER_DB_PORT?: number
  ORDER_DB_USER: string
  ORDER_DB_PASSWORD: string
  ORDER_DB_SYNCHRONIZE: boolean
}

export class EnvService {
  private vars: EnvData

  constructor () {
    const environment = process.env.NODE_ENV || 'development'
   const data: any = dotenv.parse(
      fs.readFileSync(`./../shared-env/${environment}.env`)
    )


    
    data.APP_ENV = environment
    data.APP_DEBUG = data.APP_DEBUG === 'true' ? true : false
    data.DB_PORT = parseInt(data.ORDER_DB_PORT)

    
    data.DB_SYNCHRONIZE = data.ORDER_DB_SYNCHRONIZE === 'true' ? true : false

    this.vars = data as EnvData
  }

  read (): EnvData {
    return this.vars
  }

  isDev (): boolean {
    return (this.vars.APP_ENV === 'development')
  }

  isProd (): boolean {
    return (this.vars.APP_ENV === 'production')
  }
}