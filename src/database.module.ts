import { Module, Global, DynamicModule } from '@nestjs/common'

import { TypeOrmModule } from '@nestjs/typeorm'


import { EnvModule } from './env.module'
import { EnvService } from './env.service';

function DatabaseOrmModule (): DynamicModule {
  const config = new EnvService().read()
  return TypeOrmModule.forRoot({
    type: config.ORDER_DB_TYPE,
    host: config.ORDER_DB_HOST,
    port: config.ORDER_DB_PORT,
    username: config.ORDER_DB_USER,
    password: config.ORDER_DB_PASSWORD,
    database: config.ORDER_DB_NAME,
    entities: ['dist/**/*.entity.{ts,js}'],
    synchronize:config.ORDER_DB_SYNCHRONIZE
  })
}

@Global()
@Module({
  imports: [
    EnvModule,
    DatabaseOrmModule()
  ]
})
export class DatabaseModule { }