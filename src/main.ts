import { INestMicroservice, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { join } from 'path';
import { AppModule } from './app.module';
import { protobufPackage } from './order/proto/order.pb';
import { EnvService } from './env.service';

async function bootstrap() {
  const config = new EnvService().read()
  const url = `${config.ORDER_HOST}:${config.ORDER_PORT}`;
  const app: INestMicroservice = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.GRPC,
    options: {
      url: url,
      package: protobufPackage,
      protoPath: join('node_modules/grpc-proto/proto/order.proto'),
    },
  });

  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));

  await app.listen();

  console.log(`Order Application running at ${url}`)
}

bootstrap();
